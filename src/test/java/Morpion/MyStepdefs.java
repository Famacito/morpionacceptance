package Morpion;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.it.Ma;
import org.junit.Assert;

public class MyStepdefs {
    Board board = new Board();

    @Given("la grille contient un {string} en case {int}")
    public void laGrilleContientUnEnCase(String arg0, int arg1) {

        char arg = arg0.charAt(0);

        board.setBoardPosition(arg1,arg);


    }

    @When("player {string} plays")
    public void playerPlays(String arg0) {

        char arg = arg0.charAt(0);

        int bestMove = Main.findBestMoves(board);
        board.setBoardPosition(bestMove, arg);

        Main.generateBoard(board.board);
    }

    @Then("O prend place en case {int}")
    public void oPrendPlaceEnCase(int arg0) {
        Assert.assertEquals('O',board.getBoard()[arg0]);
    }


}
